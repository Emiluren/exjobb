\documentclass[utf8]{beamer}
\mode<presentation>

\usetheme{Rochester}
\usecolortheme{spruce}
\definecolor{ming}{HTML}{417E8C}
\setbeamercolor{itemize item}{fg=ming}
\setbeamertemplate{itemize item}[circle]
\setbeamercolor{local structure}{fg=ming}

% No "Figure:" prefix for image captions
\usepackage{caption}
\captionsetup[figure]{labelformat=empty}

\usepackage{bchart} % Bar charts

% Source macro from https://tex.stackexchange.com/a/48485
\usepackage[absolute,overlay]{textpos}

\setbeamercolor{framesource}{fg=gray}
\setbeamerfont{framesource}{size=\tiny}

\newcommand{\source}[1]{\begin{textblock*}{4cm} (8.7cm,8.6cm)
    \begin{beamercolorbox}[ht=0.5cm,right]{framesource}
        \usebeamerfont{framesource}\usebeamercolor[fg]{framesource} Source: {#1}
    \end{beamercolorbox}
\end{textblock*}}

% changemargin from https://stackoverflow.com/a/1670572
\newenvironment{changemargin}[2]{%
\begin{list}{}{%
\setlength{\topsep}{0pt}%
\setlength{\leftmargin}{#1}%
\setlength{\rightmargin}{#2}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\setlength{\parsep}{\parskip}%
}%
\item[]}{\end{list}}


\title{Shape Representation Using a Volume Coverage Model}
\author{Emil Segerbäck}

\date{} % Don't show date
\setbeamertemplate{navigation symbols}{} % Clear navigation symbols

\begin{document}

\begin{frame}
  \titlepage{}
\end{frame}

\begin{frame}
  \frametitle{Background  ---  Shape representations}
  Two common mesh representations are polygon meshes and voxels.
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/Dolphin_triangle_mesh}
        \caption{A polygon mesh}
      \end{minipage}
      \hspace*{0.5cm}
      \begin{minipage}{.3\paperwidth}
        \includegraphics[width=\textwidth]{fig/Voxels}
        \source{Voxels image made by Wikimedia user Vossman CC-BY-SA}
        \caption{Voxels}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Background  ---  Distance maps}
  \begin{itemize}
    \item A distance map is a map from points in space to some distance. $d = f(x, y, z)$
      \pause{}
    \item It can be used as a shape representation by using the distance to the edge of the shape.
      \pause{}
    \item The process of creating a distance map is referred to as a distance transform.
      \pause{}
    \item I have explored creating a 3D distance map from voxel coverage data.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Background  ---  Distance maps}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/coverage}
        \caption{Area coverage of part of a circle}
      \end{minipage}
      \hspace*{0.5cm}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/distance_map}
        \caption{Distance map of part of a circle (double sided)}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Background  ---  Anti Aliasing}
  A distance transform on binary data will create jagged edges where the object edge does not align with the sampling grid.
  \begin{itemize}
    \item Grayscale data can be used to improve the result.
  \end{itemize}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/aliased_a}
        \caption{Binary data}
      \end{minipage}
      \hspace*{0.5cm}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/antialiased_a}
        \caption{Grayscale data}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Background --- Anti Aliasing}
  We want to find the distance $d$ given the volume coverage $v$.
  \begin{figure}
    \centering
    \includegraphics[height=0.9\textheight]{fig/d_explanation}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Background  ---  Precise EDT [\ldots]}
  My work is mostly based on an article called ``Precise Euclidean distance transforms in 3D from voxel coverage representation''.
  \begin{itemize}
    \item A method for anti-aliased 3D distance transforms
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Research questions}
  \pause{}
  \begin{itemize}
    \item How well does a new implementation of ``Precise EDT [\ldots]'' perform?
      \pause{}
    \item Is a numeric approach to creating the coverage model good enough?
      \pause{}
    \item How can the generated distance map be visualized?
      \pause{}
    \item Can the software developed in this project be reused?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Preparation}
  At first I implemented a 2D distance transform.
  \begin{figure}
    \includegraphics[height=.8\textheight]{fig/2d_input}
    \caption{My 2D test input data}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Preparation result}
  Generated distance maps
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/2d_dist_pos}
        \caption{Distances outside the shape}
      \end{minipage}
      \hspace*{0.5cm}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/2d_dist_neg}
        \caption{Distances inside the shape}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Preparation result}
  The implemented distance transform also outputs directional data.
  \begin{figure}
    \includegraphics[height=.8\textheight]{fig/2d_dir}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Main implementation}
  I extended my 2D DT into 3D. I made two different interfaces:
  \begin{itemize}
    \item A command line program
    \item A Rust/C library
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sphere tests}
  The implementation was tested by sampling spheres and distance transforming them.
    \pause{}
  \begin{itemize}
    \item 50 random positions to offset center
      \pause{}
    \item Radius between 1 and 30
      \pause{}
    \item Compared to the expected sphere surface distance
      \pause{}
    \item Calculated mean absolute error and error range
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Numerical sphere sampling}
  The sampling worked like this for each cell:
  \pause{}
  \begin{itemize}
    \item If the cell is inside the sphere, add the sphere's volume to coverage value
      \pause{}
    \item If the cell is on the edge of the sphere, split the cell in 8 parts and repeat
      \pause{}
    \item A small error is allowed to avoid infinite recursion
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sphere tests (MAE)}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.5\paperwidth}
        \includegraphics[width=\textwidth]{fig/mean_absolute_error}
        \caption{Mean absolute error of my results}
      \end{minipage}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/mae_precise_edt_vcedt23}
        \caption{Mean absolute error from Precise EDT [\ldots]}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Sphere tests (error range)}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.5\paperwidth}
        \includegraphics[width=\textwidth]{fig/error_range}
        \caption{Error range of my results}
      \end{minipage}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/error_range_precise_edt}
        \caption{Error range from Precise EDT [\ldots]}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Subjective evaluation}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \includegraphics[width=0.9\textwidth]{fig/teapot_survey}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Subjective evaluation result}
  \begin{figure}
    Which resolution is sufficient?
    \begin{bchart}[max=6, step=1]
      \bcbar[label=10 voxels]{0}
      \bcbar[label=15 voxels]{0}
      \bcbar[label=20 voxels]{6}
      \bcbar[label=30 voxels]{5}
    \end{bchart}
    \caption{Survey responses}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Raymarching experiment}
  \begin{changemargin}{-1cm}{-1cm}
    \begin{figure}
      \begin{minipage}{.5\paperwidth}
        \includegraphics[width=\textwidth]{fig/raymarch1}
      \end{minipage}
      \begin{minipage}{.4\paperwidth}
        \includegraphics[width=\textwidth]{fig/raymarch2}
      \end{minipage}
    \end{figure}
  \end{changemargin}
\end{frame}

\begin{frame}
  \frametitle{Conclusion}
  \begin{itemize}
    \item How well does a new implementation of ``Precise EDT [\ldots]'' perform?
      \pause{}
      \begin{itemize}
        \item The result is similar
      \end{itemize}
      \pause{}
    \item Is a numeric approach to creating the coverage model good enough?
      \pause{}
      \begin{itemize}
        \item The numeric volume coverage method works fine
      \end{itemize}
      \pause{}
    \item How can the generated distance map be visualized?
      \pause{}
      \begin{itemize}
        \item Marching cubes can be used
      \end{itemize}
      \pause{}
    \item Can the software developed in this project be reused?
      \pause{}
      \begin{itemize}
        \item The software is provided as a library to be used in other code
      \end{itemize}
  \end{itemize}
\end{frame}

\end{document}