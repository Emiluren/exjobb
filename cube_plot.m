function cube_plot(position, color)
  % CUBE_PLOT plots outline of a unit cube.
  %
  % INPUTS:
  % position = position for the cube in the form of [X, Y, Z].
  % color = color of the cube's faces.
  % OUPUTS:
  % Plot a figure.
  %
  % EXAMPLES:
  % cube_plot([2,3,4], 'white')
  %

  [X, Y, Z] = num2cell(position){:};

  vertices = [
    1 1 0;
    0 1 0;
    0 1 1;
    1 1 1;
    0 0 1;
    1 0 1;
    1 0 0;
    0 0 0
  ];

  faces = [
    1 2 3 4;
    4 3 5 6;
    6 7 8 5;
    1 2 8 7;
    6 7 1 4;
    2 3 5 8
  ];

  cube = [vertices(:, 1) + X, vertices(:, 2) + Y, vertices(:, 3) + Z];
  patch(
    'Faces', faces,
    'Vertices', cube,
    'EdgeColor', 'black',
    'FaceColor', color,
    'LineWidth', 2
  );
end

clf;
hold on;

for x = 0:2
  for y = 0:2
    cube_plot([x, y, 0], 'white');
  end
end
cube_plot([1, 1, 1], [0.5, 0.5, 0.5]);
