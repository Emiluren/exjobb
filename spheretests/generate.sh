#!/bin/bash

binary=../3d_transform/target/release/cli_transform

for radius in {1..30}
do
    echo "radius = $radius"

    random_index=0
    while read rand_pos
    do
        file="sphere_${radius}_${random_index}.h5"
        ((random_index++))

        echo "file = $file"

        $binary sample-sphere $radius $file --offset $rand_pos
        $binary transform $file
        $binary calculate-sphere-error $file $radius --offset $rand_pos
    done < random_positions
done
