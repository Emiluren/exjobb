res = zeros(1, 30);

for radius=5:28
    instance_res = zeros(1, 50);
    for i=0:49
        file = strcat('sphere_', num2str(radius) ,'_', num2str(i), '.h5');
        data = load(file);
        instance_res(i + 1) = sum(sum(sum(data.coverage)));
    end
    res(radius) = max(instance_res) - min(instance_res);
end

plot(res);
