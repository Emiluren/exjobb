error_range = zeros(1, 30);
mean_absolute_error = zeros(1, 30);

for radius=1:30
    instance_range = zeros(1, 50);
    instance_mae = zeros(1, 50);

    # Calculate errors for all the randomly offset sphere instances
    for i=1:50
        file = strcat('sphere_', num2str(radius) ,'_', num2str(i - 1), '.h5');
        data = load(file);
        instance_range(i) = max(max(max(data.difference))) - min(min(min(data.difference)));
        instance_mae(i) = mean(mean(mean(abs(data.difference))));
    end

    error_range(radius) = mean(instance_range);
    mean_absolute_error(radius) = mean(instance_mae);
end


# Plot error range
figure('NumberTitle', 'off', 'Name', 'Error range');
plot(error_range, "linewidth", 5);
set(gca, "linewidth", 4, "fontsize", 12);
ylim([0 0.5]);
xlabel('Radius');
ylabel('Range');

# Plot mean absolute error
figure('NumberTitle', 'off', 'Name', 'Mean absolute error');
plot(mean_absolute_error, "linewidth", 5);
set(gca, "linewidth", 4, "fontsize", 12);
xlabel('Radius');
ylabel('MAE');
