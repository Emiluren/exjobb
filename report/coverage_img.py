from PIL import Image, ImageFont, ImageDraw

RADIUS = 6
SIZE = RADIUS + 1

coverage_image = Image.new('L', (SIZE, SIZE))
coverage_pixels = coverage_image.load()


def coverage_at(x, y):
    local_coverage = 0
    for dx in range(16):
        for dy in range(16):
            if (x + dx/16)**2 + (y + dy/16)**2 < RADIUS**2:
                local_coverage += 1
    return local_coverage


coverage_values = [
    [coverage_at(x, y) for y in range(SIZE)] for x in range(SIZE)
]

# Create low res image
for x in range(RADIUS):
    for y in range(RADIUS):
        coverage_pixels[x, y] = coverage_values[x][y]

coverage_image = coverage_image.resize((SIZE * 100, SIZE * 100))

font = ImageFont.truetype('DejaVuSans.ttf', 50)
draw = ImageDraw.Draw(coverage_image)

# Draw grid
for y in range(RADIUS):
    y_pixel = (y+1) * 100
    draw.line([(0, y_pixel), (SIZE * 100, y_pixel)], fill=128, width=5)

for x in range(RADIUS):
    x_pixel = (x+1) * 100
    draw.line([(x_pixel, 0), (x_pixel, SIZE * 100)], fill=128, width=5)

# Draw coverage values as text
for x in range(SIZE):
    for y in range(SIZE):
        cov = coverage_values[x][y]
        color = 0 if cov >= 128 else 255
        if cov / 256 < 0.05 or cov / 256 > 0.95:
            pos = (x * 100 + 35, y * 100 + 20)
            text = '{}'.format(0 if cov / 256 < 0.5 else 1)
        else:
            pos = (x * 100 + 10, y * 100 + 20)
            text = '{0:.1f}'.format(cov / 256)
        draw.text(pos, text, font=font, fill=color)

PIXEL_RADIUS = 100 * RADIUS
draw.arc(
    [(-PIXEL_RADIUS, -PIXEL_RADIUS), (PIXEL_RADIUS, PIXEL_RADIUS)],
    0,
    90,
    255,
    width=4
)

coverage_image.save('fig/coverage.png')
