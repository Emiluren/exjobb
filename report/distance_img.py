from PIL import Image, ImageFont, ImageDraw
import math

RADIUS = 6
SIZE = RADIUS + 1

distance_image = Image.new('L', (SIZE, SIZE))
distance_pixels = distance_image.load()


def distance_at(x, y):
    center_dist = math.sqrt((x + 0.5)**2 + (y + 0.5)**2)
    return RADIUS - center_dist


max_abs = lambda xs: max(xs, key=abs)

distance_values = [
    [distance_at(x, y) for y in range(SIZE)] for x in range(SIZE)
]
max_dist = max_abs(map(max_abs, distance_values))

# Create low res image
for x in range(SIZE):
    for y in range(SIZE):
        distance_pixels[x, y] = int(abs(distance_values[x][y]) / max_dist * 255)

distance_image = distance_image.resize((SIZE * 100, SIZE * 100))

font = ImageFont.truetype('DejaVuSans.ttf', 50)
draw = ImageDraw.Draw(distance_image)

# Draw grid
for y in range(RADIUS):
    y_pixel = (y+1) * 100
    draw.line([(0, y_pixel), (SIZE * 100, y_pixel)], fill=128, width=5)

for x in range(RADIUS):
    x_pixel = (x+1) * 100
    draw.line([(x_pixel, 0), (x_pixel, SIZE * 100)], fill=128, width=5)

# Draw distance values as text
for x in range(SIZE):
    for y in range(SIZE):
        dist = abs(distance_values[x][y])
        color = 0 if dist >= max_dist / 2 else 255
        if dist < 0.05:
            pos = (x * 100 + 35, y * 100 + 20)
            text = '0'
        else:
            pos = (x * 100 + 10, y * 100 + 20)
            text = '{0:.1f}'.format(dist)
        draw.text(pos, text, font=font, fill=color)

PIXEL_RADIUS = 100 * RADIUS
draw.arc(
    [(-PIXEL_RADIUS, -PIXEL_RADIUS), (PIXEL_RADIUS, PIXEL_RADIUS)],
    0,
    90,
    255,
    width=4
)

distance_image.save('fig/distance_transform.png')
