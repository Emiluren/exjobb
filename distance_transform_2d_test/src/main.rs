use std::cmp::{min, max};
use std::f64;
use std::ops::{Add, AddAssign, Mul, Sub};
use std::path::Path;
use std::fs::File;
use std::io::prelude::*;

// fn cubic_spline_kernel(x: f64) -> f64 {
//     let a = -1.0;
//     if x < 1.0 {
//         (a + 2.0)*x.powi(3) - (a + 3.0)*x*x + 1.0
//     } else if x < 2.0 {
//         a*x.powi(3) - 5.0*a*x*x + 8.0*a*x - 4.0*a
//     } else {
//         0.0
//     }
// }

fn cubic_interpolate(t: f64, values: [f64; 4]) -> f64 {
    // TODO: maybe swap with matrix notation from
    // https://en.wikipedia.org/wiki/Bicubic_interpolation
    // to make it more readable
    values[1] + 0.5 * t*(
        values[2] - values[0] + t*(
            2.0*values[0] - 5.0*values[1] + 4.0*values[2] - values[3] + t*(
                3.0*(values[1] - values[2]) + values[3] - values[0])
        )
    )
}

fn bicubic_interpolate(tx: f64, ty: f64, values: [[f64; 4]; 4]) -> f64 {
    let mut intermediate_values = [0.0; 4];
    for x in 0..4 {
        intermediate_values[x] = cubic_interpolate(ty, values[x]);
    }
    cubic_interpolate(tx, intermediate_values)
}

trait Saturating {
    fn saturating_add(self, other: Self) -> Self;
    fn saturating_mul(self, other: Self) -> Self;
    fn saturating_sub(self, other: Self) -> Self;
}

impl Saturating for i32 {
    fn saturating_add(self, other: i32) -> i32 {
        self.saturating_add(other)
    }

    fn saturating_mul(self, other: i32) -> i32 {
        self.saturating_mul(other)
    }

    fn saturating_sub(self, other: i32) -> i32 {
        self.saturating_sub(other)
    }
}

impl Saturating for f64 {
    fn saturating_add(self, other: f64) -> f64 {
        self + other
    }

    fn saturating_mul(self, other: f64) -> f64 {
        self * other
    }

    fn saturating_sub(self, other: f64) -> f64 {
        self - other
    }
}

#[derive(Clone, Copy, Debug)]
struct Vector2<T> {
    x: T,
    y: T,
}

fn vec2<T>(x: T, y: T) -> Vector2<T> {
    Vector2 { x, y }
}

impl<T: Saturating> Add for Vector2<T> {
    type Output = Vector2<T>;

    fn add(self, other: Vector2<T>) -> Vector2<T> {
        vec2(self.x.saturating_add(other.x), self.y.saturating_add(other.y))
    }
}

impl<T> AddAssign for Vector2<T> where Vector2<T>: Add {
    fn add_assign(&mut self, other: Vector2<T>) {
        *self = other;
    }
}

impl<T: Saturating + Copy> Mul<T> for Vector2<T> {
    type Output = Vector2<T>;

    fn mul(self, factor: T) -> Vector2<T> {
        vec2(self.x.saturating_mul(factor), self.y.saturating_mul(factor))
    }
}

impl<T: Saturating> Sub for Vector2<T> {
    type Output = Vector2<T>;

    fn sub(self, other: Vector2<T>) -> Vector2<T> {
        vec2(self.x.saturating_sub(other.x), self.y.saturating_sub(other.y))
    }
}

impl<T: Saturating + Copy> Vector2<T> {
    fn squared_length(self) -> T {
        T::saturating_add(
            self.x.saturating_mul(self.x),
            self.y.saturating_mul(self.y)
        )
    }

    // fn min(self, other: Vector2<T>) -> Vector2<T> where T: PartialOrd {
    //     if self.squared_length() < other.squared_length() { self } else { other }
    // }

    fn length(self) -> f64 where T: Into<f64> {
        (self.squared_length().into()).sqrt()
    }
}

impl From<Vector2<i32>> for Vector2<f64> {
    fn from(v: Vector2<i32>) -> Vector2<f64> {
        vec2(v.x as f64, v.y as f64)
    }
}

impl From<Vector2<i32>> for Vector2<usize> {
    fn from(v: Vector2<i32>) -> Vector2<usize> {
        vec2(v.x as usize, v.y as usize)
    }
}

impl From<Vector2<i32>> for Vector2<u32> {
    fn from(v: Vector2<i32>) -> Vector2<u32> {
        vec2(v.x as u32, v.y as u32)
    }
}

impl<T> Into<(T, T)> for Vector2<T> {
    fn into(self) -> (T, T) {
        (self.x, self.y)
    }
}

impl Vector2<f64> {
    fn normalize(self) -> Vector2<f64> {
        let len = self.length();
        self * (1.0 / len)
    }
}

type Vec2D<T> = Vec<Vec<T>>;

// compute_gradient, edgedf, distaa3 were originally written by Stefan Gustavsson
// as C code http://weber.itn.liu.se/~stegu76/edtaa/
// This version is converted to rust.

/*
 * Compute the local gradient at edge pixels using convolution filters.
 * The gradient is computed only at edge pixels. At other places in the
 * image, it is never used, and it's mostly zero anyway.
 */
fn compute_gradient(img: &image::GrayImage) -> Vec2D<Vector2<f64>> {
    let sqrt_2 = 2.0_f64.sqrt();
    let mut gradients = vec![vec![vec2(0.0, 0.0); img.height() as usize]; img.width() as usize];

    for x in 1..(img.width() - 1) { // Avoid edges where the kernels would spill over
        for y in 1..(img.height() - 1) {
            if img[(x, y)].data[0] > 0 && img[(x, y)].data[0] < 255 { // Compute gradient for edge pixels only
                let pix_float = |x, y| (img[(x, y)].data[0] as f64) / 255.0;
                let gradient = vec2(
                    -pix_float(x-1,y-1) - sqrt_2*pix_float(x-1,y) - pix_float(x-1,y+1) + pix_float(x+1,y-1) + sqrt_2*pix_float(x+1,y) + pix_float(x+1,y+1),
                    -pix_float(x-1,y-1) - sqrt_2*pix_float(x,y-1) - pix_float(x+1,y-1) + pix_float(x-1,y+1) + sqrt_2*pix_float(x,y+1) + pix_float(x+1,y+1),
                );
                if gradient.squared_length() > 0.0 { // Avoid division by zero
                    gradients[x as usize][y as usize] = gradient.normalize();
                }
            }
        }
    }

    // TODO: Compute reasonable values for gx, gy also around the image edges.
    // (These are zero now, which reduces the accuracy for a 1-pixel wide region
    // around the image edge.) 2x2 kernels would be suitable for this.

    gradients
}

/*
 * A somewhat tricky function to approximate the distance to an edge in a
 * certain pixel, with consideration to either the local gradient (gx,gy)
 * or the direction to the pixel (dx,dy) and the pixel greyscale value a.
 * The latter alternative, using (dx,dy), is the metric used by edtaa2().
 * Using a local estimate of the edge gradient (gx,gy) yields much better
 * accuracy at and near edges, and reduces the error even at distant pixels
 * provided that the gradient direction is accurately estimated.
 */
fn edgedf(mut gradient: Vector2<f64>, area_coverage: f64) -> f64
{
    if (gradient.x == 0.0) || (gradient.y == 0.0) { // Either A) gx or gy are zero, or B) both
        0.5 - area_coverage // Linear approximation is A) correct or B) a fair guess
    } else {
        /* Everything is symmetric wrt sign and transposition,
         * so move to first octant (gx>=0, gy>=0, gx>=gy) to
         * avoid handling all possible edge directions.
         */
        gradient = vec2(gradient.x.abs(), gradient.y.abs());
        if gradient.x < gradient.y {
            gradient = vec2(gradient.y, gradient.x)
        }

        let a1 = 0.5 * gradient.y / gradient.x;
        if area_coverage < a1 { // 0 <= a < a1
            0.5 * (gradient.x + gradient.y) - (2.0 * gradient.x * gradient.y * area_coverage).sqrt()
        } else if area_coverage < (1.0 - a1) { // a1 <= a <= 1-a1
            (0.5 - area_coverage) * gradient.x
        } else { // 1-a1 < a <= 1
            -0.5 * (gradient.x + gradient.y) +
                (2.0 * gradient.x * gradient.y * (1.0 - area_coverage)).sqrt()
        }
    }    
}

// Calculate floating point distance to edge given the edge pixel's area
// coverage, gradient and the integer vector to it
fn distaa3(mut coverage: f64, gradient: Vector2<f64>, closest_vector: Vector2<i32>) -> f64 {
    // Clip grayscale values outside the range [0,1]
    coverage = coverage.min(1.0).max(0.0);
    if coverage == 0.0 {
        return f64::INFINITY; // Not an object pixel, return "very far" ("don't know yet")
    } else if coverage == 1.0 {
        return f64::NEG_INFINITY;
    }

    let closest_distance = closest_vector.length(); // Length of integer vector, like a traditional EDT
    let subpixel_distance = if closest_distance == 0.0 { // Use local gradient only at edges
        // Estimate based on local gradient only
        edgedf(gradient, coverage)
    } else {
        let closest_float_vec: Vector2<f64> = closest_vector.into();
        // Estimate gradient based on direction to edge (accurate for large di)
        edgedf(closest_float_vec.normalize(), coverage)
    };
    return closest_distance + subpixel_distance.abs(); // Same metric as edtaa2, except at edges (where di=0)
}

// Calculate floating point coverage from the distance to do the inverse for
// reconstruction
fn coverage_from_distance(mut gradient: Vector2<f64>, distance: f64) -> f64 {
    if distance < 0.0 {
        return 1.0 - coverage_from_distance(gradient, -distance);
    }

    gradient = vec2(gradient.x.abs(), gradient.y.abs());
    if gradient.x < gradient.y {
        gradient = vec2(gradient.y, gradient.x);
    }

    let a1 = 0.5 * gradient.y / gradient.x;
    let a2 = 1.0 - 2.0 * a1;
    let d1 = gradient.y;
    let d2 = 1.0 / 2.0_f64.sqrt() * (std::f64::consts::PI/4.0 - gradient.y.asin());

    if distance >= d1 + d2 {
        0.0
    } else if distance >= d2 {
        (d1 + d2 - distance).powi(2) * a1 / d1.powi(2)
    } else {
        a1 + a2/2.0*(1.0 - distance/d2)
    }
}

fn antialiased_8ssed(
    img: &image::GrayImage,
    gradients: &Vec2D<Vector2<f64>>
) -> (Vec2D<f64>, Vec2D<Vector2<i32>>) {
    // Start by setting all empty elements to 0 and filled ones to f64::MAX
    let initial_dist = |x, y| {
        let pixel_val = img[(x, y)].data[0];

        if pixel_val == 0 {
            f64::INFINITY
        } else if pixel_val < 255 {
            edgedf(gradients[x as usize][y as usize], pixel_val as f64 / 255.0)
        } else {
            f64::NEG_INFINITY
        }
    };

    let mut distance_map: Vec2D<_> = (0..img.width()).map(
        |x| (0..img.height()).map(|y| initial_dist(x, y)).collect()
    ).collect();

    let mut integer_distance =
        vec![vec![vec2(0, 0); img.height() as usize]; img.width() as usize];

    // (0, 0) is skipped because it is used as the default value in the fold
    let mask1 = vec![
        vec2(-1, -1), vec2(0, -1), vec2(1, -1),
        vec2(-1,  0)
    ];
    let mask2 = vec![vec2(1, 0)];

    let (iwidth, iheight) = (img.width() as i32, img.height() as i32);

    let in_bounds =
        |v: Vector2<i32>| v.x >= 0 && v.x < iwidth && v.y >= 0 && v.y < iheight;

    let y_dirs = [
        (1, 0),
        (-1, iheight - 1),
    ];
    let x_dirs = [
        (1, 0, &mask1),
        (-1, iwidth - 1, &mask2)
    ];

    let mut changes_made = true;
    while changes_made {
        changes_made = false;

        for (y_dir, mut y) in y_dirs.iter().cloned() {
            while y >= 0 && y < iheight {
                for (x_dir, mut x, mask) in &x_dirs {
                    while x >= 0 && x < iwidth {
                        let (xu, yu) = (x as usize, y as usize);

                        for offset in mask.iter().cloned().map(|v| v * y_dir) {
                            let neighbor = vec2(x, y) + offset;

                            if in_bounds(neighbor) {
                                let neighbor_edge_dist = integer_distance[neighbor.x as usize][neighbor.y as usize];
                                let edge: Vector2<u32> = (neighbor + neighbor_edge_dist).into();

                                let i_new_dist = neighbor_edge_dist + offset;
                                let new_dist = distaa3(
                                    img[(edge.x, edge.y)].data[0] as f64 / 255.0,
                                    gradients[edge.x as usize][edge.y as usize],
                                    i_new_dist
                                ) * distance_map[xu][yu].signum();

                                if new_dist.abs() < distance_map[xu][yu].abs() {
                                    distance_map[xu][yu] = new_dist;
                                    integer_distance[xu][yu] = i_new_dist;
                                }
                            }
                        }
                        x += x_dir
                    }
                }
                y += y_dir;
            }
        }
    }

    (distance_map, integer_distance)
}

fn main() {
    let img = image::open(&Path::new("figure.png"))
        .expect("Could not open figure.png")
        .to_luma();

    let gradients = compute_gradient(&img);

    // Eight-point sequential signed euclidean distance mapping
    let (distance_map, integer_distance) = antialiased_8ssed(&img, &gradients);

    let res_factor = 2.0;
    let (output_width, output_height) = (
        (img.width() as f64 * res_factor).ceil() as u32,
        (img.height() as f64 * res_factor).ceil() as u32
    );

    let mut output_buf = image::ImageBuffer::new(output_width, output_height);

    let mut dm_values = vec![0.0; (output_width * output_height) as usize];
    for x in 0..output_width {
        for y in 0..output_height {
            let sampling_x = x as f64 / res_factor;
            let sampling_y = y as f64 / res_factor;

            let (sx, sy) = (sampling_x as i32, sampling_y as i32);

            // Clamp surrounding values to edge
            let mut surrounding_xs = [sx - 1, sx, sx + 1, sx + 2];
            let mut surrounding_ys = [sy - 1, sy, sy + 1, sy + 2];
            for i in 0..4 {
                let x = surrounding_xs[i];
                let y = surrounding_ys[i];
                surrounding_xs[i] = max(0, min(img.width() as i32 - 1, x));
                surrounding_ys[i] = max(0, min(img.height() as i32 - 1, y));
            }

            let mut surrounding_values = [[0.0; 4]; 4];
            for xi in 0..4 {
                for yi in 0..4 {
                    let (x, y) = (surrounding_xs[xi] as usize, surrounding_ys[yi] as usize);
                    surrounding_values[xi][yi] = distance_map[x][y];
                }
            }

            let (tx, ty) = (sampling_x - sx as f64, sampling_y - sy as f64);
            let dist = bicubic_interpolate(tx, ty, surrounding_values) * res_factor;
            let edge_dir = vec2(
                integer_distance[sx as usize][sy as usize].x as f64,
                integer_distance[sx as usize][sy as usize].y as f64,
            );

            dm_values[(y * output_width + x) as usize] = dist;

            let cov = coverage_from_distance(edge_dir.normalize(), dist);
            let pixel_value = (cov * 255.0) as u8;
            output_buf.put_pixel(x, y, image::Luma([pixel_value]));
        }
    }

    let max_dist = dm_values.iter().cloned().fold(f64::NEG_INFINITY, f64::max);
    let min_dist = dm_values.iter().cloned().fold(f64::INFINITY, f64::min);

    let normalized_dm_values_pos: Vec<_> = dm_values.iter().cloned().map(
        |v| (v / max_dist * 255.).max(0.) as u8
    ).collect();
    let normalized_dm_values_neg: Vec<_> = dm_values.iter().cloned().map(
        |v| (v / min_dist * 255.).max(0.) as u8
    ).collect();

    output_buf.save("output.png").unwrap();
    image::save_buffer(
        "distance_map_pos.png", &normalized_dm_values_pos, output_width, output_height, image::Gray(8)
    ).unwrap();
    image::save_buffer(
        "distance_map_neg.png", &normalized_dm_values_neg, output_width, output_height, image::Gray(8)
    ).unwrap();

    let mut dir_file = File::create("edge_direction.dat").unwrap();
    let dir_step = 3;

    for x in 0..img.width() as usize / dir_step {
        for y in 0..img.height() as usize / dir_step {
            let dx = integer_distance[x * dir_step][y * dir_step].x;
            let dy = integer_distance[x * dir_step][y * dir_step].y;
            dir_file.write_all(
                format!(
                    "{}\t{}\t{}\t{}\n",
                    x * dir_step,
                    img.height() as usize - (y+1)*dir_step,
                    dx,
                    -dy,
                ).as_bytes()
            ).unwrap();
        }
    }
}
