\documentclass[
  utf8,%     More capable input encoding than latin-1.
  % parskip,%  For vertical whitespace between paragraphs.  This comes down to more than just using parskip.sty, so it's better to use this class option.
  % S5MP % If you intend to really use margin paragraphs (not recommended!).
%  crop,%     Produce output with crop marks and paper size A4.  Liu-Tryck should like this.  Automatically adds information, including the physical page number, at the top of each page.
       %     Add option 'noInfo' to suppress the info at the top of each page when using option 'crop'.
  % Font options: 'kp' (default), 'times', 'lm'.  The KpFonts (loaded using 'kp'), is the most complete font among the provided options.  Among other, it supports slanted small caps.  See rtthesis.cls for more details regarding the font options.
  largesmallcaps,intlimits,widermath,% Good options to KpFonts.
  sharecounter,nobreak,definition=marks,%  See comments in the rtthesisex results chapter of this document for more information on these options!
  numbers, % If you want to cite references by numbers, use this option.
  noparts% Use option 'noparts' if you do not make use of part divisions.
]{rtthesis}

\usepackage{pgfgantt}
\usepackage{todonotes}

\setupThesis{
  author={Emil Segerbäck},
  title={Shape representation using a volume coverage model},
  subtitle={},
  swetitle={},
  swesubtitle={},
  city=Linköping,
  year=2019,
  type=msc,
  username=emise935,
  subject={Computer Graphics},
  department=isy,
  division={Information Coding},
  examiner={Ingemar Ragnemalm \AT\textsc{isy}, Linköpings universitet},
  supervisor={Stefan Gustavson \AT\textsc{itn}, Linköpings universitet},
  keywords={},
  isrn=LiTH-ISY-EX-{}-YY/NNNN-{}-SE,
  url={http://urn.kb.se/resolve?urn=urn:nbn:se:liu:diva-XXXXX},
  dedication={}
}

\begin{document}
\selectlanguage{english}

\frontmatter
\maketitle

\tableofcontents

\mainmatter{}
\chapter{Introduction}
A distance transform (DT) is a way of representing shapes in an arbitrary number of dimensions. Basically one can represent the shape as a grid where each point contains the distance to the edge of the shape. Potentially this can reduce the necessary resolution compared to a pixel or voxel representation. A better result can be achieved if the voxel data contains some information about partial coverage of elements, a form of antialiasing.

In this thesis the usage of distance transforms generated from a volume coverage model will be investigated. The goal is to build on previous research on the this subject and compare some different approaces. To create a coverage model from a polygonal mesh either a analytical method or a numeric method can be used to calculate the coverage of partially covered elements. The numeric approach is significantly simpler so this project will examine if it is sufficiently accurate. There are a few different approaces to calculating the distance transform from a coverage model so the difference between them will be examined.

There are several possible applications of distance transforms such as erosion, dilation, opening, closing, shape matching and skeletonization/thinning of shapes. If there is time available this project will test some of these using the result.

\section{Research questions}
The thesis tackles the following questions/tasks:
\begin{enumerate}
\item How do the already known methods of calculating a distance transform compare?
\item Can the known methods be improved to find a more accurate result?
\item Is a numeric approach to creating the coverage model good enough?
\item How can a polygonal mesh best be constructed from the distance transform?
\end{enumerate}

\chapter{Theory}
In this chapter there will be a summary of some of the theory that will be used in the thesis. Coverage models are explained in Section~\ref{coverage} and distance transforms in Section~\ref{distance transforms}.

\section{Coverage model}\label{coverage}
In a pixel or voxel representation the value in each element can be used as a factor of how much that element is covered, see Figure~\ref{fig:coverage}. A value of 0 would mean that the element does not contain any part of the object. Vice versa, a value of 1 means that the element is completely within the object. The interesting elements are those with a value between 0 and 1 because that is where the edge is located which is the area of the object we want to improve the accuracy of.

\begin{figure}
  \centering
  \includegraphics{fig/coverage}
  \caption{Area coverage of part of a circle}\label{fig:coverage}
\end{figure}

\section{Distance transform}\label{distance transforms}
A distance transform is a mapping from each image point to its smallest distance to a selected subset of image points. This subset can for example be the border of a shape. See Figure~\ref{fig:DT} for a distance transform generated from part of the image in Figure~\ref{fig:coverage}.

The simplest form of a distance transform would be created from a pure binary image, one where every element is either completely filled or empty. This however produces a result with jagged edges in those cases where the edges are not aligned with the sampling grid. To get a better result the ``greyscale'' coverage data can be used.

\begin{figure}
  \centering
  \includegraphics{fig/distance_transform}
  \caption{Distance transform of part of a circle}\label{fig:DT}
\end{figure}

\section{Prior research}\label{prior research}
Under this section a number of research papers are collected and discussed in relation to this thesis.

\subsection{Euclidean distance mapping}
This paper contains an early algorithm for computing an approximate euclidean distance transform based on vector propagation. This algorithm uses a pure binary image as its input~\cite{Danielsson:1980}. Many of the later algorithms are based on this one.

\subsection{Anti-aliased Euclidean distance transform}
In this paper the authors present a distance measure that has a better accuracy for lower resolution images than earlier methods~\cite{Gustavson:2011}. The method uses area coverage to improve the precision.

\subsection{Precise Euclidean transforms in 3D from voxel coverage representation}
In this paper the authors present two algorithms for estimating Euclidean DT with sub-voxel precision~\cite{Ilic:2015}.

\subsection{Exact Linear Time Euclidean Distance Transforms of Grid Line Sampled Shapes}
In this paper the authors propose a method for computing the Euclidean distance transform. This method does not use a coverage representation as input however but rather a ``Grid line sampling''~\cite{Lindblad:2015}. This is supposed to have a similar result but be faster and easier to control/evaluate than the method found by~\citet{Ilic:2015}. The algorithm doesn't take coverage data as its input but the paper gives an explanation of how grid line sampling can be approximated using the coverage data.

\subsection{3D distance fields: a survey of techniques and applications}
This paper contains a survey comparing different algorithms that compute three dimensional euclidean distance transforms.

\chapter{Time plan}
This chapter contains an estimated plan for the thesis. By the time of the half time review, a working 3D distance transform should have been implemented. Possibly also together with some coverage based algorithm that enhances the result.

\begin{table}
  \begin{ganttchart}[hgrid, vgrid, expand chart=\textwidth]{1}{20}
    \gantttitle{Week planning for the project}{20} \\
    \gantttitlelist{1,...,20}{1} \\
    \ganttbar{Planning}{1}{4} \\
    \ganttbar{Understand DT basics}{1}{4} \\
    \ganttbar{Implement simple 2D DT}{3}{5} \\
    \ganttbar{Implement 3D DT}{6}{8} \\
    \ganttbar{Try some improvements}{9}{12} \\
    \ganttbar{Experiment with applications}{13}{15} \\

    \ganttmilestone{Half-time thesis review}{09} \\

    \ganttbar{Report: Introduction}{4}{6} \\
    \ganttbar{Report: Theory}{06}{09} \\
    \ganttbar{Report: Method}{10}{13} \\
    \ganttbar{Report: Results}{14}{15} \\
    \ganttbar{Report: Discussion}{16}{17} \\
    \ganttbar{Report: Finalise}{18}{19} \\
    \ganttbar{Prepare presentation}{18}{19} \\
    \ganttbar{Opposition}{20}{20}
  \end{ganttchart}
  \caption{Gantt chart of the time plan}\label{gantt}
\end{table}

\section{Activities}
Table~\ref{gantt} contains a Gantt chart of the plan. The different activities are explained in detail below.

\subsection{Planning}
Planning the thesis work. This is mostly made up of writing this document.

\subsection{Read relevant litterature}
A vital component of this work is that the basics are fully understood.

\subsection{Implement simple 2D DT}
To make sure that the basics are understood a simple distance transform in two dimensions will be implemented. Possibly only using binary data and no coverage data.

\subsection{Implement 3D DT}
After the 2D distance transform has been implemented, it can be extended to work in three dimensions.

\subsection{Try some improvements}
The papers introduced in Section~\ref{prior research} mostly deal with algorithms that improve upon a more basic transform. So these will be used to see how the result improves. There should be some difference between different methods, such as accuracy, computational complexity and fitness for parallellization.

\subsection{Experiment with applications}
Some of the possible applications of distance transforms such as erosion, dilation, opening, closing and skeletonization/thinning of shapes will be tested with the different methods.

\section{Risk analysis}
There is always a risk of things not going exactly as planned. The following subsections will try to anticipate some of the possible problems that might appear during the project.

\subsection{Difficult to implement}
The code may be hard to implement. This could lead to a lot of time being put into debugging.

\subsection{Unfamiliar subject}
The work requires quite a lot of knowledge in the area of distance transforms and may require a lot of time spent reading articles.

\backmatter{}

\bibliographystyle{plainnat}
\bibliography{sources}

\printindex

\end{document}