#!/bin/bash

binary=../target/release/distance_transform_3d

for size in {1..40}
do
    echo "size = $size"

    file="teapot_${size}.h5"

    echo "file = $file"

    $binary sample-obj ../teapot.obj $file --resolution $size
    $binary transform $file
done
